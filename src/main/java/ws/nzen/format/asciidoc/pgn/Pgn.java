
package ws.nzen.format.asciidoc.pgn;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.asciidocj.AsciiDocProcessor;

public class Pgn
{
	public static void main( String[] args )
	{
		if ( args.length < 1 )
		{
			return; // NOTE nothing to convert
		}
		String fromPath = args[ 0 ];
		String toPath = args[ 1 ];
		try
		{
			Path from = Paths.get( fromPath );
			Path to = Paths.get( toPath );
			String entireFile = new String( Files.readAllBytes( from ) );
			AsciiDocProcessor adpMagic = new AsciiDocProcessor();
			String html = adpMagic.asciidocToHtml( entireFile );
			if ( html == null || html.isEmpty() )
			{
				System.out.println( "blank result" );
				return;
			}
			Files.write( to, html.getBytes() );
		}
		catch ( Exception any )
		{
			System.out.println( any );
		}
	}
}

